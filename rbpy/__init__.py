# -*- coding: utf-8 -*-

__title__ = 'rbpy'
__version__ = '0.1.1'
__author__ = 'Matt Maybeno'
__license__ = 'BSD'
__copyright__ = 'Copyright 2016 Matt Maybeno'

from .relay import Box
