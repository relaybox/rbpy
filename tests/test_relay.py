# -*- coding: utf-8 -*-
from __future__ import (
    absolute_import,
    division,
    print_function,
    unicode_literals)
import json
import unittest
try:
    from unittest import mock
except ImportError:
    import mock
from rbpy import relay


class TestRelay(unittest.TestCase):

    def setUp(self):
        api_key = 'foo'.encode(encoding='utf-8')
        pub_id = 'bar'
        self.rb = relay.Box(api_key, pub_id)

    def test_relayboxurl(self):
        self.assertIn('{', self.rb.relaybox_url)
        self.assertNotIn('{', self.rb.relaybox_url.format(type='get'))
        self.assertNotIn('{', self.rb.relaybox_url.format(type='send'))

    def test_make_header(self):
        json_body = json.dumps({})
        header = self.rb.make_header(json_body)
        self.assertDictEqual(
            {'Content-Type': 'application/json',
             'X-APP-API-VERSION': '1.0.0',
             'X-APP-HASH': 'c76356efa19d219d1d7e08ccb20b1d26'
                           'db53b143156f406c99dcb8e0876d6c55',
             'X-APP-PUB': 'bar'}, header)

    @mock.patch('rbpy.relay.Box.send', autospec=True)
    def test_send(self, mock_send_message):
        mock_return = {"success": True, "id": "foobar"}
        mock_send_message.return_value = mock_return
        r = self.rb.send('hello world', public=True)
        self.assertDictEqual(mock_return, r)

    @mock.patch('rbpy.relay.Box.get', autospec=True)
    def test_get(self, mock_get_message):
        mock_return = {
            "data": {
                "0": {
                    "timestamp": "2016-01-01 00:00:00",
                    "message": "hello world",
                    "id": "LPek5o5rGRIB",
                    "public": 1
                }
            },
            "success": True}

        mock_get_message.return_value = mock_return
        r = self.rb.get()
        self.assertDictEqual(mock_return, r)
